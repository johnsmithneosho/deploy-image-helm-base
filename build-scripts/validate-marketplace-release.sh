#!/usr/bin/env bash

# Import build environment variables and shared functions
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

if ! verify_registry_defined; then
    display_failure "Nope Nope Nope"
fi

if [ -n "${GL_MP_LOADBALANCER_IP}" ]; then
    load_balancer=", \"global.hosts.externalIP\": \"${GL_MP_LOADBALANCER_IP}\""
fi

parameter="{\"APP_INSTANCE_NAME\": \"${GL_MP_APP_INSTANCE_NAME}\",\"NAMESPACE\": \"${GL_MP_NAMESPACE}\", \"global.hosts.domain\": \"${GL_MP_TLD}\"${load_balancer}}"

display_task "Attempting to verify deployer image"

IFS=" " read -r -a tags <<< "$(get_container_tags 'REMOTE_ONLY')"
tag="${tags[0]}"

if mpdev /scripts/verify --deployer="${tag}" --parameters="${parameter}"; then
    display_success "Deployer image tagged ${tag} verified"
else
    display_failure "Deployer image ${tag} invalid."
fi
